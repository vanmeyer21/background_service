import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

final FlutterLocalNotificationsPlugin flutterLocalPlugin = FlutterLocalNotificationsPlugin();

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initservice();
  runApp(const MyApp());
}

Future<void> initservice() async {
  var service = FlutterBackgroundService();
  //set for ios
  if (Platform.isIOS) {
    await flutterLocalPlugin.initialize(const InitializationSettings(iOS: DarwinInitializationSettings()));
  }

  // //service init and start
  await service.configure(
    iosConfiguration: IosConfiguration(onBackground: iosBackground, onForeground: onStart),
    androidConfiguration: AndroidConfiguration(onStart: onStart, autoStart: false, isForegroundMode: true, foregroundServiceNotificationId: 90),
  );

  //for ios enable background fetch from add capability inside background mode
}

//onstart method
@pragma("vm:entry-point")
void onStart(ServiceInstance service) {
  DartPluginRegistrant.ensureInitialized();

  service.on("setAsForeground").listen((event) {
    print("foreground ===============");
  });

  service.on("setAsBackground").listen((event) {
    print("background ===============");
  });

  service.on("stopService").listen((event) {
    service.stopSelf();
  });


  //display notification as service
  Timer.periodic(const Duration(seconds: 2), (timer) {
    print("${DateTime.now()}");
    flutterLocalPlugin.show(90, "Cool Service", "Awsome ${DateTime.now()}",
        NotificationDetails(android: AndroidNotificationDetails("coding is life", "${DateTime.now()}", ongoing: true, icon: "ic_launcher")));
  });
}

//iosbackground
@pragma("vm:entry-point")
Future<bool> iosBackground(ServiceInstance service) async {
  WidgetsFlutterBinding.ensureInitialized();
  DartPluginRegistrant.ensureInitialized();

  return true;
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            //layout
            const SizedBox(
              height: 200,
            ),
            ElevatedButton(
                onPressed: () {
                  FlutterBackgroundService().invoke("stopService");
                },
                child: const Text("stop service")),
            ElevatedButton(
                onPressed: () {
                  FlutterBackgroundService().startService();
                },
                child: const Text("start service")),
          ],
        ),
      ),
    );
  }
}
